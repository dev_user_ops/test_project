Создание CI/CD конвейера для веб-приложения https://github.com/shekshuev/projectzero. Написание dockerfile для backend, реализованный на Java Spring и организация взаимодействия БД PostgreSQL с backend с использованием docker-compose


## Run local
```shell
export $(cat .env | xargs)  
mvn install
mvn package
java --jar target/projectzero-0.0.1-SNAPSHOT.jar
```

## Deploy to Heroku

```shell
heroku login
heroku container:login
heroku container:push web --app=<app_name>
heroku container:release web --app=<app_name>
```
